import React from 'react';
import PropTypes from 'prop-types';

import Label from './Label';
import StyledCheckbox, { Wrapper } from './styles/Checkbox.style';

const Checkbox = ({ id, label, className, ...props }) => {
  return (
    <Wrapper className={className} {...props}>
      <StyledCheckbox id={id} {...props} />
      <Label htmlFor={id}>{label}</Label>
    </Wrapper>
  );
};

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  label: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string
};

Checkbox.defaultProps = {
  label: '',
  size: 'md',
  disabled: false,
  className: ''
};

Checkbox.displayName = 'Form.Checkbox';
Checkbox.wrapperStyle = Wrapper;
Checkbox.style = StyledCheckbox;

export default Checkbox;
