import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledFormGroup, { contexts } from './styles/FormGroup.style';

const FormGroup = ({ context, className, ...props }) => {
  const classes = classnames(className, {
    [`has-${context}`]: context !== ''
  });

  return <StyledFormGroup className={classes} {...props} />;
};

FormGroup.propTypes = {
  context: PropTypes.oneOf(contexts),
  className: PropTypes.string
};

FormGroup.defaultProps = {
  context: '',
  className: ''
};

FormGroup.displayName = 'Form.Group';
FormGroup.style = StyledFormGroup;
FormGroup.contexts = contexts;

export default FormGroup;
