import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  color: ${props => props.theme.form.label.color};
  background-color: ${props => props.theme.form.label.backgroundColor};
  border-color: ${props => props.theme.form.label.borderColor};
`;

const StyledRadio = styled.input.attrs({
  type: 'radio',
  checked: props => props.checked,
  disabled: props => props.disabled
})`
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
  margin: 0;

  & + label {
    font-size: ${props => props.theme.form.radio[props.size].size};
    line-height: ${props => props.theme.form.radio[props.size].lineHeight};
    font-weight: ${props => props.theme.form.label.fontWeight};
    padding-left: ${props => props.theme.form.radio[props.size].paddingLeft};
    cursor: pointer;
    transition: opacity 0.2s ease-in-out;

    &::before,
    &::after {
      display: inline-block;
      position: absolute;
      left: 0;
      top: ${props => props.theme.form.radio[props.size].top};
      border: 1px solid #ccc;
      border-radius: 50%;
      border-color: ${props => props.theme.form.radio.borderColor};
      content: '';
      width: ${props => props.theme.form.radio[props.size].size};
      height: ${props => props.theme.form.radio[props.size].size};
    }

    &::after {
      border: none;
      content: ' ';
      width: ${props => props.theme.form.radio[props.size].innerSize};
      height: ${props => props.theme.form.radio[props.size].innerSize};
      top: ${props => props.theme.form.radio[props.size].innerTop};
      left: ${props => props.theme.form.radio[props.size].innerLeft};
      background-color: ${props => props.theme.form.radio.backgroundColor};
      opacity: 0;
      transition: opacity 0.1s ease-in-out;
    }
  }

  &:checked + label::after {
    opacity: 1;
  }

  &:disabled + label,
  &:disabled + label::after {
    opacity: ${props => props.theme.form.radio.disabledOpacity};
    cursor: not-allowed;
  }
`;

export default StyledRadio;
