import React from 'react';
import styled from 'styled-components';

const Form = props => <form {...props} />;

Form.propTypes = {};
Form.defaultProps = {};

Form.displayName = 'Form';
Form.style = styled(Form);

export default Form;
