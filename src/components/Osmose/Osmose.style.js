import styled from 'styled-components';

const StyledOsmose = styled.div`
  position: relative;

  color: black;
  height: 90vh;
  background-color: ${p => p.theme.backgroundColor};

  & > div {
    height: 100%;
    padding-bottom: 2rem;
    padding-top: ${p => p.theme.titlebar.lgHeight};
    overflow-y: scroll;
  }

  h3 {
    text-align: center;
    margin-top: 1rem;
    font-size: 1.5rem;
  }
`;

export default StyledOsmose;
