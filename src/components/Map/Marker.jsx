import React from 'react';
import L from 'leaflet';
import PropTypes from 'prop-types';
import { Marker } from 'react-leaflet';
import { injectGlobal } from 'styled-components';

import shapes from 'assets/markers';
import { buildMarkerIconColor } from 'helpers/themes';
import './Marker.style';

const injectedColors = [];

const MapMarker = ({
  theme,
  color,
  shape,
  position,
  icon,
  // iconType,
  ...props
}) => {
  let base64Color = '';

  if (color) {
    base64Color = btoa(color).replace(/(=)/g, '');

    if (!injectedColors.includes(base64Color)) {
      injectedColors.push(base64Color);
      const iconColor = buildMarkerIconColor(color);

      /* eslint-disable no-unused-expressions */
      injectGlobal`
        .osm-ui-react-marker-shape.${base64Color} {
          svg #colorized,
          svg #colorized * {
            ${color ? `fill: ${color} !important;` : ''}
          }

          .osm-ui-react-marker-icon-wrapper i {
            ${color ? `color: ${iconColor} !important;` : ''}
          }
        }
      `;
    }
  }

  return (
    <Marker
      position={position}
      icon={L.divIcon({
        className: `osm-ui-react-marker-shape
        osm-ui-react-marker-${theme}
        osm-ui-react-marker-${shape}
        ${base64Color}
      `,
        iconAnchor: shapes[shape].iconAnchor,
        html: `
        ${shapes[shape].raw}
        <div class="osm-ui-react-marker-icon-wrapper">
            <i class="fa fa-${icon}"></i>
        </div>
      `
      })}
      {...props}
    />
  );
};

const shapeNames = Object.keys(shapes);

MapMarker.propTypes = {
  position: PropTypes.arrayOf(PropTypes.number).isRequired,
  theme: PropTypes.string,
  color: PropTypes.string,
  shape: PropTypes.oneOf(shapeNames),
  icon: PropTypes.string
  // iconType: PropTypes.oneOf(['font-awesome']),
};

MapMarker.defaultProps = {
  theme: 'default',
  color: '',
  shape: shapeNames[0],
  icon: ''
  // iconType: 'font-awesome',
};

MapMarker.displayName = 'Map.Marker';

export default MapMarker;
