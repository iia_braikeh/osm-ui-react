import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { StyledTitle } from './Sidebar.style';

const Title = ({ inHeader, className, children, ...rest }) => {
  const classes = classnames(className, {
    'sidebar-title': true,
    'in-header': inHeader
  });

  return (
    <StyledTitle className={classes} {...rest}>
      {children}
    </StyledTitle>
  );
};

Title.propTypes = {
  inHeader: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Title.defaultProps = {
  inHeader: false,
  className: ''
};

Title.displayName = 'Sidebar.Title';
Title.style = StyledTitle;

export default Title;
