import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { StyledHeader } from './Sidebar.style';

const Header = ({ className, children, ...rest }) => (
  <StyledHeader className={classnames(className, 'sidebar-header')} {...rest}>
    {children}
  </StyledHeader>
);

Header.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Header.defaultProps = {
  className: ''
};

Header.displayName = 'Sidebar.Header';
Header.style = StyledHeader;

export default Header;
