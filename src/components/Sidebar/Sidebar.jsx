import React from 'react';
import PropTypes from 'prop-types';
import CSSTransition from 'react-transition-group/CSSTransition';
import classnames from 'classnames';

import Loader from 'components/Loader';

import SidebarTitle from './Title';
import StyledSidebar, { slideDuration } from './Sidebar.style';

class Sidebar extends React.Component {
  componentWillMount() {
    this.setState({ opened: this.props.opened });
  }

  componentDidMount() {
    if (this.props.onOpen) this.props.onOpen();

    if (this.props.maximized === true) {
      this.props.onMaximize();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.maximized !== nextProps.maximized) {
      if (nextProps.maximized === true) this.props.onMaximize();
      else this.props.onUnmaximize();
    }

    if (this.state.opened !== nextProps.opened) {
      this.setState({ opened: nextProps.opened });
    }
  }

  componentWillUnmount() {
    if (this.props.onClose) this.props.onClose();
  }

  _handleBackClick() {
    if (this.props.onClickBack) this.props.onClickBack();
  }

  _handleCloseClick() {
    this.setState({ opened: false });

    if (this.props.onClickClose) this.props.onClickClose();
    if (this.props.onClose) this.props.onClose();
  }

  render() {
    const {
      title,
      header,
      footer,
      children,
      loading,
      loaderLabel,
      position,
      width,
      maximized,
      container,
      scrollContent,
      className,
      ...rest
    } = this.props;

    const asideClasses = classnames(className, {
      [position]: true,
      [width]: true,
      maximized,
      [`container-${container}`]: true,
      'scroll-content': scrollContent
    });

    const contentClasses = classnames({
      content: true,
      loading
    });

    return (
      <CSSTransition
        classNames="slide"
        appear
        mountOnEnter
        unmountOnExit
        in={this.state.opened}
        timeout={slideDuration}
      >
        <StyledSidebar key="sidebar" className={asideClasses} {...rest}>
          <header className="header">
            {this.props.onClickBack && (
              <button
                className="back-btn"
                onClick={() => this._handleBackClick()}
              >
                <i className="fas fa-chevron-left" />
              </button>
            )}
            <button
              className="close-btn"
              onClick={() => this._handleCloseClick()}
            >
              <i className="fas fa-times" />
            </button>
            {title && <SidebarTitle inHeader>{title}</SidebarTitle>}
            <div className="clearfix" />
            {!loading && header && header}
          </header>

          <div className={contentClasses}>{children}</div>

          {!loading && footer && footer}

          {loading && <Loader centered label={loaderLabel} />}
        </StyledSidebar>
      </CSSTransition>
    );
  }
}

Sidebar.propTypes = {
  title: PropTypes.string,
  header: PropTypes.node,
  footer: PropTypes.node,
  loading: PropTypes.bool,
  loaderLabel: PropTypes.node,
  position: PropTypes.oneOf(['left', 'right']),
  width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg']),
  opened: PropTypes.bool,
  maximized: PropTypes.bool,
  container: PropTypes.oneOf(['parent', 'root']),
  scrollContent: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  onClickClose: PropTypes.func,
  onClickBack: PropTypes.func,
  onMaximize: PropTypes.func,
  onUnmaximize: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Sidebar.defaultProps = {
  title: '',
  header: '',
  footer: '',
  loading: false,
  loaderLabel: '',
  position: 'left',
  width: 'md',
  maximized: false,
  opened: true,
  container: 'parent',
  scrollContent: false,
  onOpen: null,
  onClickClose: null,
  onClose: null,
  onClickBack: null,
  onMaximize: null,
  onUnmaximize: null,
  className: ''
};

Sidebar.displayName = 'Sidebar';
Sidebar.style = StyledSidebar;

export default Sidebar;
