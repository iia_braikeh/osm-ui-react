import styled from 'styled-components';

export const StyledFooter = styled.div`
  padding: 2rem 2rem;
`;

export const StyledHeader = styled.div`
  padding: 1.5rem 2rem 0;
`;

export const StyledNav = styled.nav`
  margin: 2rem 0;

  ul {
    text-align: center;
    list-style: none;
    margin: 0 0 4rem;
    padding: 0;
  }

  a {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    text-decoration: none;

    display: block;
    margin: 0.5rem 0;
    padding: 0.9rem 1.6rem;
    border-radius: ${p => p.theme.sidebar.nav.borderRadius};
    background-color: ${p => p.theme.sidebar.nav.backgroundColor};
    color: ${p => p.theme.sidebar.nav.color};
    font-size: ${p => p.theme.sidebar.nav.fontSize};
    line-height: ${p => p.theme.sidebar.nav.lineHeight};
    font-weight: ${p => p.theme.sidebar.nav.fontWeight};
  }

  a:hover,
  a:active,
  a:focus {
    text-decoration: none;
    color: ${p => p.theme.sidebar.nav.hoverColor};
    background-color: ${p => p.theme.sidebar.nav.hoverBackgroundColor};
  }
`;

export const StyledTitle = styled.h2`
  margin: -3rem 0 3rem;

  &.in-header {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin: 0 0 0 2rem;
    line-height: 3rem;
  }
`;

export const slideDuration = 300;

const StyledSidebar = styled.aside`
  z-index: 1000;
  top: 0;
  max-width: 100%;
  height: 100%;
  overflow-y: auto;
  transition: all ${slideDuration}ms ease-out;
  font-size: ${p => p.theme.sidebar.fontSize};
  line-height: ${p => p.theme.sidebar.lineHeight};

  color: ${p => p.theme.color};
  background: ${p => p.theme.backgroundColor};
  box-shadow: ${p => p.theme.sidebar.boxShadow};

  &.scroll-content {
    display: flex;
    flex-direction: column;
    overflow-y: none;
  }

  &.container-parent {
    position: absolute;
  }
  &.container-root {
    position: fixed;
  }

  &.xs {
    width: 15rem;
  }
  &.sm {
    width: 20rem;
  }
  &.md {
    width: 30rem;
  }
  &.lg {
    width: 40rem;
  }
  &.maximized {
    width: 100%;
  }

  &.left {
    left: 0;
    border-right-width: ${p => p.theme.borderWidth};
  }

  &.right {
    right: 0;
    border-left-width: ${p => p.theme.borderWidth};
  }

  &.left.slide-appear,
  &.left.slide-enter,
  &.left.slide-exit.slide-exit-active,
  &.left.slide-exit.slide-exit-done {
    transform: translate(-110%, 0);
  }

  &.right.slide-appear,
  &.right.slide-enter,
  &.right.slide-exit.slide-exit-active,
  &.right.slide-exit.slide-exit-done {
    transform: translate(110%, 0);
  }

  &.left.slide-appear.slide-appear-active,
  &.left.slide-enter.slide-enter-active,
  &.left.slide-enter.slide-enter-done,
  &.left.slide-exit,
  &.right.slide-appear.slide-appear-active,
  &.right.slide-enter.slide-enter-active,
  &.right.slide-enter.slide-enter-done,
  &.right.slide-exit {
    transform: translate(0, 0);
  }

  &.left.maximized,
  &.right.maximized {
    border-width: 0;
  }

  .back-btn,
  .close-btn {
    color: ${p => p.theme.controlColor};
    background: transparent;
    border-width: 0;
    width: 3rem;
    height: 3rem;
    padding: 0;

    &:hover {
      color: ${p => p.theme.hoverControlColor};
    }
  }

  .back-btn {
    float: left;
    margin-right: 0.5rem;
  }

  .close-btn {
    float: right;
    margin-left: 0.5rem;
  }

  &.scroll-content .header {
    margin-bottom: 2rem;
  }

  .content {
    margin-top: 3rem;
    padding: 2rem;
  }

  &.scroll-content .content {
    overflow-y: auto;
    margin: 0;
    border-color: ${p => p.theme.borderColor};
    border-style: ${p => p.theme.borderStyle};
    border-width: 1px 0 1px 0;

    &::after {
      content: '';
      margin-top: 2rem;
      display: block;
    }
  }

  .content.loading {
    display: none;
  }

  &.scroll-content .footer {
    margin-top: 2rem;
  }
`;

export default StyledSidebar;
