import React from 'react';

import { StyledNav } from './Sidebar.style';

const Nav = props => <StyledNav {...props} />;

Nav.propTypes = {};
Nav.defaultProps = {};

Nav.displayName = 'Sidebar.Nav';
Nav.style = StyledNav;

export default Nav;
