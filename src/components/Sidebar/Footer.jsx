import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { StyledFooter } from './Sidebar.style';

const Footer = ({ className, children, ...rest }) => (
  <StyledFooter className={classnames(className, 'sidebar-footer')} {...rest}>
    {children}
  </StyledFooter>
);

Footer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};

Footer.defaultProps = {
  className: ''
};

Footer.displayName = 'Sidebar.Footer';
Footer.style = StyledFooter;

export default Footer;
