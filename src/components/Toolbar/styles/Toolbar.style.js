import styled from 'styled-components';

const StyledToolbar = styled.aside`
  z-index: 1000;
  transition: all 0.1s ease-out;

  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  padding: ${p => p.theme.toolbar.margin};
  pointer-events: none;

  &.container-parent {
    position: absolute;
  }
  &.container-root {
    position: fixed;
  }

  &.direction-row {
    flex-direction: row;

    &.position-top-left,
    &.position-center-left,
    &.position-bottom-left {
      justify-content: flex-start;
    }

    &.position-top-center,
    &.position-center-center,
    &.position-bottom-center {
      justify-content: center;
    }

    &.position-top-right,
    &.position-center-right,
    &.position-bottom-right {
      justify-content: flex-end;
    }

    &.position-top-left,
    &.position-top-center,
    &.position-top-right {
      align-items: flex-start;
    }

    &.position-center-left,
    &.position-center-center,
    &.position-center-right {
      align-items: center;
    }

    &.position-bottom-left,
    &.position-bottom-center,
    &.position-bottom-right {
      align-items: flex-end;
    }
  }

  &.direction-column {
    flex-direction: column;

    &.position-top-left,
    &.position-top-center,
    &.position-top-right {
      justify-content: flex-start;
    }

    &.position-center-left,
    &.position-center-center,
    &.position-center-right {
      justify-content: center;
    }

    &.position-bottom-left,
    &.position-bottom-center,
    &.position-bottom-right {
      justify-content: flex-end;
    }

    &.position-top-left,
    &.position-center-left,
    &.position-bottom-left {
      align-items: flex-start;
    }

    &.position-top-center,
    &.position-center-center,
    &.position-bottom-center {
      align-items: center;
    }

    &.position-top-right,
    &.position-center-right,
    &.position-bottom-right {
      align-items: flex-end;
    }
  }

  &.direction-column {
    &.position-top-left,
    &.position-center-left,
    &.position-bottom-left {
      transform: translate(-150%, 0);
    }

    &.position-top-center {
      transform: translate(0, -150%);
    }

    &.position-bottom-center {
      transform: translate(0, 150%);
    }

    &.position-top-right,
    &.position-center-right,
    &.position-bottom-right {
      transform: translate(150%, 0);
    }
  }

  &.direction-row {
    &.position-top-left,
    &.position-top-center,
    &.position-top-right {
      transform: translate(0, -150%);
    }

    &.position-center-left {
      transform: translate(-150%, 0);
    }

    &.position-center-right {
      transform: translate(150%, 0);
    }

    &.position-bottom-left,
    &.position-bottom-center,
    &.position-bottom-right {
      transform: translate(0, 150%);
    }
  }

  &.direction-column,
  &.direction-row {
    &.position-center-center {
      opacity: 0;
    }
  }

  &.direction-column > * {
    margin-bottom: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-row > * {
    margin-right: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-column > *:last-child,
  &.direction-row > *:last-child {
    margin: 0;
  }
`;

export default StyledToolbar;
