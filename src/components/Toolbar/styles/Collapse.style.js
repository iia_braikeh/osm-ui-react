import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;

  &.direction-row {
    flex-direction: row;
  }
  &.direction-column {
    flex-direction: column;
  }
`;

const Collapse = styled.div`
  transition: all 0.1s ease-out;

  display: flex;

  &.direction-row {
    flex-direction: row;
  }
  &.direction-column {
    flex-direction: column;
  }

  &.direction-column.position-center-left > *,
  &.direction-column.position-top-left > *,
  &.direction-column.position-top-center > *,
  &.direction-column.position-top-right > *,
  &.direction-column.position-center-right > * {
    margin-top: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-column.position-bottom-left > *,
  &.direction-column.position-bottom-center > *,
  &.direction-column.position-bottom-right > * {
    margin-bottom: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-row.position-top-center > *,
  &.direction-row.position-top-left > *,
  &.direction-row.position-center-left > *,
  &.direction-row.position-bottom-left > *,
  &.direction-row.position-bottom-center > * {
    margin-left: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-row.position-top-right > *,
  &.direction-row.position-center-right > *,
  &.direction-row.position-bottom-right > * {
    margin-right: ${p => p.theme.toolbar.childrenMargin};
  }

  &.direction-column {
    &.position-top-left,
    &.position-center-left,
    &.position-bottom-left {
      transform: translate(-150%, 0);
    }

    &.position-top-center {
      transform: translate(0, -200%);
    }

    &.position-bottom-center {
      transform: translate(0, 200%);
    }

    &.position-top-right,
    &.position-center-right,
    &.position-bottom-right {
      transform: translate(150%, 0);
    }
  }

  &.direction-row {
    &.position-top-left,
    &.position-top-center,
    &.position-top-right {
      transform: translate(0, -150%);
    }

    &.position-center-left {
      transform: translate(-200%, 0);
    }

    &.position-center-right {
      transform: translate(200%, 0);
    }

    &.position-bottom-left,
    &.position-bottom-center,
    &.position-bottom-right {
      transform: translate(0, 150%);
    }
  }

  &.direction-column,
  &.direction-row {
    &.position-top-left,
    &.position-top-center,
    &.position-top-right,
    &.position-center-right,
    &.position-bottom-right,
    &.position-bottom-center,
    &.position-bottom-left,
    &.position-center-left {
      &.opened {
        transform: translate(0, 0);
      }
    }
  }
`;

export default Collapse;
