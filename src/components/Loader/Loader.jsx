import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import StyledLoader, { Wrapper, Label } from './Loader.style';

const Loader = ({
  centered,
  spinnerSize,
  strokeSize,
  label,
  className,
  ...rest
}) => {
  const classes = classnames(className, {
    centered
  });

  return (
    <Wrapper className={classes} {...rest}>
      <div>
        <StyledLoader spinnerSize={spinnerSize} strokeSize={strokeSize} />
        {label && <Label>{label}</Label>}
      </div>
    </Wrapper>
  );
};

Loader.propTypes = {
  centered: PropTypes.bool,
  spinnerSize: PropTypes.number,
  strokeSize: PropTypes.number,
  label: PropTypes.string,
  className: PropTypes.string
};

Loader.defaultProps = {
  centered: false,
  spinnerSize: 40,
  strokeSize: 3,
  label: '',
  className: ''
};

Loader.displayName = 'Loader';
Loader.style = Wrapper;

export default Loader;
