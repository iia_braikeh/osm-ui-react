import styled, { keyframes } from 'styled-components';

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const StyledLoader = styled.div`
  width: ${p => p.spinnerSize / p.theme.rem}rem;
  height: ${p => p.spinnerSize / p.theme.rem}rem;
  margin: 0 auto;
  border-width: ${p => p.strokeSize}px;
  border-style: solid;
  border-color: ${p => p.theme.loaderColor};
  border-radius: 50%;
  border-left-color: transparent;
  border-bottom-color: transparent;
  border-right-color: transparent;
  animation: ${rotate360} 650ms infinite linear;
`;

export const Label = styled.div`
  color: ${p => p.theme.loaderColor};
  margin-top: 1.5rem;
`;

export const Wrapper = styled.div`
  background-color: ${p => p.theme.backgroundColor};
  text-align: center;

  &.centered {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export default StyledLoader;
