import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import Editor from '..';
import { osmData } from 'helpers/__mocks__/osm';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(Editor, {
      original: osmData.original,
      fixed: osmData.fixed
    }));
});
