import React from 'react';
import { storiesOf } from '@storybook/react';
import { host } from 'storybook-host';
import { withKnobs } from '@storybook/addon-knobs';
import defaultHostOptions from 'helpers/__stories__/defaultHostOptions';

import { DefaultTheme, Editor } from 'index';

import { osmData } from 'helpers/__mocks__/osm';

storiesOf('Osm', module)
  .addDecorator(withKnobs)
  .addDecorator(
    host({
      ...defaultHostOptions,
      title: 'Osm'
    })
  )
  .addWithInfo('Field', () => (
    <DefaultTheme>
      <Editor.Field tag="original" value="Value is unchanged" />
      <Editor.Field tag="changed" status="mod" value="Value has changed" />
      <Editor.Field tag="new" status="add" value="Field is new" />
      <Editor.Field tag="removed" status="del" value="Field has been removed" />
      <Editor.Field tag="emptyTag" />
    </DefaultTheme>
  ))
  .addWithInfo('Editor', () => (
    <DefaultTheme>
      <Editor original={osmData.original} fixed={osmData.fixed} />
    </DefaultTheme>
  ));
