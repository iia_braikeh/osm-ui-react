import styled from 'styled-components';

import Button from 'components/Button';
import Form from 'components/Form';
import { colors } from 'constants/index';

const StyledEditor = styled.div`
  max-width: 40rem;
  margin: 0 auto;

  .removedList {
    margin-top: 3rem;

    & > div:first-child {
      margin-bottom: 1rem;
      font-size: 2rem;
      font-weight: bold;
    }
  }

  .add-item {
    transition: opacity 300ms ease-in-out;

    .input-container {
      display: inline-block;
    }

    &.inactive {
      opacity: 0.2;
    }

    .add-button-container {
      display: inline-block;
      vertical-align: top;
      background-color: ${colors.white};
      margin-left: -15rem;
      transition: margin-left 300ms ease-in-out, width 300ms ease-in-out;
    }

    .input-container,
    .add-button-container {
      width: 15rem;
    }

    span {
      transition: transform 300ms ease-in-out;
    }

    &.open {
      ${Button.style} {
        border-radius: 0 2px 2px 0;
      }

      span {
        transform: rotate(45deg);
      }

      .add-button-container {
        width: 0;
        margin-left: 0;
      }
    }

    ${Form.Input.style} {
      opacity: 1;
      line-height: 1.6rem;
      padding: 1px 1rem;
      font-size: 0.9rem;
      font-weight: 500;
      color: ${colors.white};
      background-color: ${colors.blue3};
      border-color: ${colors.blue1};
      border-right: none;
      border-radius: 2px 0 0 2px;

      &::placeholder {
        color: ${colors.blue5};
        font-weight: normal;
      }
    }
  }

  ${Button.style} {
    padding-left: 1rem;
    padding-right: 1rem;

    &:focus {
      outline: none;
    }

    &.submit {
      margin-top: 3rem;
    }
  }
`;

export default StyledEditor;
