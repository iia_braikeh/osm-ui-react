import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import IconPicker from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () => {
    snapshotWithElementChildren(IconPicker);
  });
});
