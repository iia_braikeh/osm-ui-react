import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import ColorPicker from '..';

describe('When using snapshots', () => {
  it('Should render with an element children', () =>
    snapshotWithElementChildren(ColorPicker, { onChange: jest.fn() }));
});
