import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TwitterPicker } from 'react-color';

const ColorPicker = ({ className, onChange, ...rest }) => {
  return (
    <div className={className} {...rest}>
      <TwitterPicker triangle="hide" onChangeComplete={onChange} />
    </div>
  );
};

ColorPicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string
};

ColorPicker.defaultProps = {
  className: ''
};

ColorPicker.displayName = 'ColorPicker';
ColorPicker.style = styled(ColorPicker);

export default ColorPicker;
