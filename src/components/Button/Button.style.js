import styled from 'styled-components';

export const contexts = [
  'default',
  'primary',
  'info',
  'success',
  'warning',
  'danger',
  'link'
];

const colorsStyle = props =>
  contexts.reduce((reducedStyles, context) => {
    const isLink = context === 'link';
    const colors =
      context === 'default'
        ? props.theme.form.button
        : props.theme.form.button[context];

    return `
      ${reducedStyles}

      &.btn-${context} {
        font-weight: ${colors.fontWeight};
        color: ${colors.color};
        background-color: ${colors.backgroundColor};
        ${isLink ? 'cursor: pointer;' : ''}

        &:hover {
          background-color: ${colors.hoverBackgroundColor};
          ${isLink ? 'text-decoration: underline;' : ''}
        }

        &:focus {
          outline: none;
          border-color: ${colors.outlineColor};
          background-color: ${colors.focusBackgroundColor};
          ${isLink ? 'text-decoration: underline;' : ''}
        }

        &:active {
          ${!isLink ? 'border-color: transparent;' : ''}
          background-color: ${colors.activeBackgroundColor};
          ${isLink ? 'text-decoration: none;' : ''}
        }
      }
    `;
  }, '');

const StyledButton = styled.button`
  border: 2px solid transparent;
  border-radius: ${p => p.theme.form.button.borderRadius};

  /* prettier-ignore */
  ${p => colorsStyle(p)}

  &:disabled,
  &.disabled {
    opacity: .6;
  }

  &.btn-block {
    display: block;
    width: 100%;
  }

  &.btn-xs {
    font-size: 0.7rem;
    line-height: 1.2rem;
    padding-left: 0.7rem;
    padding-right: 0.7rem;
  }

  &.btn-sm {
    font-size: 0.8rem;
    line-height: 1.6rem;
    padding-left: 0.9rem;
    padding-right: 0.9rem;
  }

  &.btn-md {
    font-size: 0.9rem;
    line-height: 2.2rem;
    padding-left: 1.2rem;
    padding-right: 1.2rem;
  }

  &.btn-lg {
    font-size: 1.2rem;
    line-height: 2.6rem;
    padding-left: 1.4rem;
    padding-right: 1.4rem;
  }

  &.shape-round {
    &.btn-xs {
      border-radius: 1.4rem;
    }

    &.btn-sm {
      border-radius: 1.6rem;
    }

    &.btn-md {
      border-radius: 2.2rem;
    }

    &.btn-lg {
      border-radius: 2.6rem;
    }
  }
`;

export const StyledAnchor = styled(StyledButton.withComponent('a'))`
  display: inline-block;
  text-decoration: none;
`;

export default StyledButton;
