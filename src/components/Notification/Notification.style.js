import styled from 'styled-components';

export const contexts = ['info', 'success', 'warning', 'danger'];

export const Wrapper = styled.div`
  position: absolute;
  width: ${p => p.theme.notification.width};
  max-width: 100%;

  @media (max-width: 50rem) {
    width: 100%;
  }

  transition: transform 500ms ease-in-out;

  &.position-top-right {
    top: 0;
    right: 0;
    transform: translate(100%, 0);

    &.direction-vertical {
      transform: translate(0, -100%);
    }
  }

  &.position-top-left {
    top: 0;
    left: 0;
    transform: translate(-100%, 0);

    &.direction-vertical {
      transform: translate(0, -100%);
    }
  }

  &.position-bottom-left {
    bottom: 0;
    left: 0;
    transform: translate(-100%, 0);

    &.direction-vertical {
      transform: translate(0, 100%);
    }
  }

  &.position-bottom-right {
    bottom: 0;
    right: 0;
    transform: translate(100%, 0);

    &.direction-vertical {
      transform: translate(0, 100%);
    }
  }

  &.position-top {
    top: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
    transform: translate(100%, 0);

    &.direction-vertical {
      transform: translate(0, -100%);
    }
  }

  &.position-bottom {
    bottom: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
    transform: translate(100%, 0);

    &.direction-vertical {
      transform: translate(0, 100%);
    }
  }
`;

const StyledNotification = styled.aside`
  position: relative;
  display: flex;
  z-index: 1000;
  width: 100%;
  height: ${p => p.theme.notification.height};
  margin: 0.4rem 0;
  padding: 0 4rem 0 2rem;
  overflow-y: auto;
  font-size: ${p => p.theme.notification.fontSize};
  color: ${p => p.theme.notification[p.context].color};
  background: ${p => p.theme.notification[p.context].backgroundColor};
  box-shadow: ${p => p.theme.sidebar.boxShadow};

  transition: height 500ms ease-in-out, margin 500ms ease-in-out;

  .close-btn {
    display: inline-block;
    position: absolute;
    top: 0;
    right: 0;
    width: ${p => p.theme.notification.height};
    height: ${p => p.theme.notification.height};
    line-height: ${p => p.theme.notification.height};
    padding: 0;
    color: ${p => p.theme.notification[p.context].controlColor};
    background: none;
    border: none;

    &:hover {
      color: ${p => p.theme.notification[p.context].hoverControlColor};
    }
  }

  .content {
    display: inline-flex;
    flex: 2 1 auto;
    height: 100%;
    align-items: center;
    justify-content: space-between;

    & * {
      margin-left: 0.5rem;
    }
  }
`;

export default StyledNotification;
