import styled from 'styled-components';

const StyledGroup = styled.aside`
  position: absolute;
  display: flex;
  flex-direction: column;
  width: 50rem;
  @media (max-width: 50rem) {
    width: 100%;
  }

  &:empty {
    padding-top: 0;
    padding-bottom: 0;
  }

  &.position-top-right {
    top: 0;
    right: 0;
    justify-content: flex-end;
  }

  &.position-top-left {
    top: 0;
    left: 0;
    justify-content: flex-start;
  }

  &.position-bottom-right {
    bottom: 0;
    right: 0;
    justify-content: flex-end;
    flex-direction: column-reverse;
  }

  &.position-bottom-left {
    bottom: 0;
    left: 0;
    justify-content: flex-start;
    flex-direction: column-reverse;
  }

  &.position-top {
    top: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
  }

  &.position-bottom {
    bottom: 0;
    left: 0;
    right: 0;
    width: 50%;
    margin-left: auto;
    margin-right: auto;
    flex-direction: column-reverse;
  }

  & .notif-item {
    position: relative;
    width: 100%;
  }
`;

export default StyledGroup;
