import React from 'react';
import 'jest-styled-components';
import { snapshotWithoutChildren } from 'helpers/tests';
import OffMapMarker from '..';

describe('When using snapshots', () => {
  it('Should render with the default props', () =>
    snapshotWithoutChildren(OffMapMarker));

  it('Should render with custom props', () =>
    snapshotWithoutChildren(OffMapMarker, {
      icon: 'home',
      shape: 'pointerClassicThin',
      theme: 'sky'
    }));
});
