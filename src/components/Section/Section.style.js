import styled from 'styled-components';

const StyledSection = styled.section`
  color: ${props => props.theme.color};
  background-color: ${props => props.theme.backgroundColor};

  & > * {
    opacity: ${props => (props.dimmed ? 0.2 : 1)};
  }

  &.app-canvas {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
  }
`;

export default StyledSection;
