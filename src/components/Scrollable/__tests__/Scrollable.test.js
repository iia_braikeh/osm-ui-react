import 'jest-styled-components';
import { snapshotWithElementChildren } from 'helpers/tests';
import Scrollable from '..';

jest.mock('helpers/dom');

describe('When using snapshots', () => {
  it('Should render with element children ', () => {
    snapshotWithElementChildren(Scrollable);
  });
});
