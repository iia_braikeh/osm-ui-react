import styled from 'styled-components';

const StyledScrollable = styled.div`
  height: 100%;
  color: ${props => props.theme.color};

  & > div {
    height: 100%;
    overflow: scroll;
  }

  &::before,
  &::after {
    font-family: 'Font Awesome 5 Free';
    font-size: 0.8rem;
    position: absolute;
    left: ${props => (props.position === 'left' ? '3px' : 'unset')};
    right: ${props => (props.position === 'right' ? '3px' : 'unset')};
    z-index: 1000;

    font-weight: 900;
  }

  &.up::before {
    content: '\f106';
    top: 0;
  }

  &.down::after {
    content: '\f107';
    bottom: 0;
  }
`;

export default StyledScrollable;
