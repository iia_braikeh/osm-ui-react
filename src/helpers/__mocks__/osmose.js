export const addData = {
  maxlon: 165.90897,
  subtitle:
    'Terrain de volley-ball, Terrain de Volley-Ball de Oua Tom, Terrain de Volley de la Tribu de Oua Tom',
  elems: [],
  minlat: -21.79516,
  b_date: '2018-03-11',
  elems_id: '',
  maxlat: -21.79116,
  lat: '-21.7931600',
  new_elems: [
    {
      add: [
        {
          k: 'leisure',
          v: 'pitch'
        },
        {
          k: 'source',
          v:
            'data.gouv.fr:Le ministère des droits des femmes, de la ville, de la jeunesse et des sports - 11/2015'
        },
        {
          k: 'sport',
          v: 'volleyball'
        },
        {
          k: 'surface',
          v: 'asphalt'
        }
      ],
      num: 0,
      del: [],
      mod: []
    }
  ],
  minlon: 165.90497,
  title: 'Pitch not integrated Terrain de volley-ball',
  url_help: 'http://wiki.openstreetmap.org/wiki/Osmose/errors',
  lon: '165.9069700',
  item: 8170,
  error_id: 16567449854
};

export const fixData = {
  maxlon: -61.590266799999995,
  subtitle:
    'École primaire Louis Andrea 1, RUE EUTROPE MARIAN, 97122, BAIE MAHAULT (positioned: building, matching: manual)',
  elems: [
    {
      node: true,
      fixes: [
        {
          add: [
            {
              k: 'ref:UAI',
              vlink:
                'http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?lycee_name=9710194J',
              v: '9710194J'
            },
            {
              k: 'school:FR',
              v: 'primaire'
            },
            {
              k: 'source',
              v: 'data.gouv.fr:Éducation Nationale - 05/2016'
            }
          ],
          num: 0,
          del: [],
          mod: []
        },
        {
          add: [
            {
              k: 'ref:UAI',
              vlink:
                'http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?lycee_name=9710194J',
              v: '9710194J'
            },
            {
              k: 'school:FR',
              v: 'primaire'
            },
            {
              k: 'source',
              v: 'data.gouv.fr:Éducation Nationale - 05/2016'
            }
          ],
          num: 1,
          del: ['amenity'],
          mod: [
            {
              k: 'name',
              v: 'École primaire Louis Andrea 1'
            }
          ]
        }
      ],
      type: 'node',
      id: 4685618649,
      tags: [
        {
          k: 'amenity',
          v: 'school'
        },
        {
          k: 'name',
          v: 'Collège Maurice SATINEAU'
        }
      ]
    }
  ],
  minlat: 16.2581911,
  b_date: '2018-03-11',
  elems_id: 'node4685618649',
  maxlat: 16.2621911,
  lat: '16.2601911',
  new_elems: [],
  minlon: -61.5942668,
  title: 'School, integration suggestion',
  url_help: 'http://wiki.openstreetmap.org/wiki/Osmose/errors',
  lon: '-61.5922668',
  item: 8031,
  error_id: 16567871739
};

export const fixData2 = {
  maxlon: -0.5828402,
  subtitle:
    'École élémentaire David Johnston, 44 RUE DAVID JOHNSTON, 33000, BORDEAUX (position : bâtiment, appariement : manuel)',
  elems: [
    {
      fixes: [
        {
          add: [],
          num: 0,
          del: [],
          mod: [
            {
              k: 'school:FR',
              v: 'primaire'
            }
          ]
        },
        {
          add: [],
          num: 1,
          del: [],
          mod: [
            {
              k: 'name',
              v: 'École élémentaire David Johnston'
            },
            {
              k: 'school:FR',
              v: 'primaire'
            }
          ]
        }
      ],
      type: 'way',
      id: 192089068,
      way: true,
      tags: [
        {
          k: 'amenity',
          v: 'school'
        },
        {
          k: 'name',
          v: 'École élementaire David Johnston'
        },
        {
          k: 'ref:UAI',
          vlink:
            'http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?lycee_name=0330468E',
          v: '0330468E'
        },
        {
          k: 'school:FR',
          v: 'élémentaire'
        },
        {
          k: 'source',
          v:
            "data.gouv.fr:Ministère de l'Éducation nationale, de la Jeunesse et de la Vie associative - 05/2012"
        }
      ]
    }
  ],
  minlat: 44.848621099999995,
  b_date: '2017-04-30',
  elems_id: 'way192089068',
  maxlat: 44.8526211,
  lat: '44.8506211',
  new_elems: [],
  minlon: -0.5868402,
  title: 'École mise à jour',
  url_help: 'http://wiki.openstreetmap.org/wiki/FR:Osmose/erreurs',
  lon: '-0.5848402',
  item: 8032,
  error_id: 26086704
};
