import hexRgb from 'hex-rgb';
import _merge from 'lodash/merge';
import { darken, getLuminance, lighten } from 'polished';
import React from 'react';
import { injectGlobal, ThemeProvider } from 'styled-components';

export function themeFactory(config) {
  return ({ children, ...rest }) => (
    <ThemeProvider theme={config} {...rest}>
      <React.Fragment>
        {React.Children.map(children, child => {
          if (React.isValidElement(child)) {
            return React.cloneElement(child, rest);
          }

          return child;
        })}
      </React.Fragment>
    </ThemeProvider>
  );
}

export function makeTransparent(hexColor, opacity) {
  const colorAsAnArray = hexRgb(hexColor);
  colorAsAnArray.push(opacity);
  return `rgba(${colorAsAnArray.join(',')})`;
}

export function buildMarkerIconColor(color) {
  const luminance = getLuminance(color);

  if (luminance > 0.4) {
    return darken(0.7, color);
  } else {
    return lighten(0.7, color);
  }
}

export function buildDarkThemeConfig(baseConfig, colors, colorVariant) {
  const color1 = colors[`${colorVariant}1`];
  const color2 = colors[`${colorVariant}2`];
  const color3 = colors[`${colorVariant}3`];
  // const color4 = colors[`${colorVariant}4`];
  const color5 = colors[`${colorVariant}5`];

  /* eslint-disable no-unused-expressions */
  injectGlobal`
    .osm-ui-react-marker-${colorVariant} {
      color: ${colors.white};

      #colorized, #colorized * {
        fill: ${color2} !important;
      }
    }

    .osm-ui-react-marker-cluster-${colorVariant} > div {
      color: ${colors.white};
      background: ${color2};
    }
  `;
  /* eslint-enable */

  return _merge({}, baseConfig, {
    color: colors.white,
    backgroundColor: color2,
    borderColor: color1,
    loaderColor: colors.white,
    controlColor: color1,
    hoverControlColor: colors.white,

    marker: {
      color: colors.white,
      fill: color2
    },
    sidebar: {
      nav: {
        color: colors.white,
        backgroundColor: lighten(0.1, color2),
        hoverColor: colors.white,
        hoverBackgroundColor: color3
      }
    },
    toolbar: {
      button: {
        color: colors.white,
        backgroundColor: color2,
        borderColor: color1,
        hoverBackgroundColor: color3,
        hoverBorderColor: color2,
        focusBackgroundColor: color3,
        focusBorderColor: color2,
        activeBackgroundColor: color2,
        activeBorderColor: color2
      }
    },
    titlebar: {
      color: colors.white,
      backgroundColor: color2,

      button: {
        color: colors.white,
        backgroundColor: color2,
        hoverBackgroundColor: color3,
        focusBackgroundColor: color3,
        activeBackgroundColor: color3
      }
    },
    form: {
      button: {
        color: colors.white,
        backgroundColor: color1,
        hoverBackgroundColor: color3,
        focusBackgroundColor: color3,
        activeBackgroundColor: color1,
        outlineColor: color1,

        primary: {
          color: color1,
          backgroundColor: colors.white,
          hoverBackgroundColor: color5,
          focusBackgroundColor: color5,
          activeBackgroundColor: colors.white,
          outlineColor: colors.white
        },

        link: {
          color: colors.white,
          outlineColor: color1
        }
      },
      label: {
        color: colors.white,
        backgroundColor: color2,
        borderColor: color5,
        hoverBackgroundColor: color3,
        hoverBorderColor: color5,
        focusBackgroundColor: color3,
        focusBorderColor: color5,
        activeBackgroundColor: color3,
        activeBorderColor: color5
      },
      checkbox: {
        color: colors.white,
        backgroundColor: color2,
        borderColor: color5,
        hoverBackgroundColor: color3,
        hoverBorderColor: color5,
        focusBackgroundColor: color3,
        focusBorderColor: color5,
        activeBackgroundColor: color3,
        activeBorderColor: color5
      }
    },
    modal: {
      color: colors.white,
      backgroundColor: color2
    },
    list: {
      color: colors.white,
      backgroundColor: color2,
      borderColor: color5
    }
  });
}

export function buildLightThemeConfig(baseConfig, colors, colorVariant) {
  const color1 = colors[`${colorVariant}1`];
  const color2 = colors[`${colorVariant}2`];
  const color3 = colors[`${colorVariant}3`];
  const color4 = colors[`${colorVariant}4`];
  const color5 = colors[`${colorVariant}5`];
  const dark18Color1 = darken(0.18, color1);
  const dark22Color1 = darken(0.22, color1);
  const dark27Color1 = darken(0.27, color1);

  /* eslint-disable no-unused-expressions */
  injectGlobal`
    .osm-ui-react-marker-${colorVariant} {
        color: ${dark27Color1};

        #colorized, #colorized * {
            fill: ${color2} !important;
        }
    }

    .osm-ui-react-marker-cluster-${colorVariant} > div {
      color: ${dark27Color1};
      background: ${color2};
    }
  `;
  /* eslint-enable */

  return _merge({}, baseConfig, {
    color: dark27Color1,
    backgroundColor: color2,
    borderColor: color1,
    loaderColor: dark27Color1,
    controlColor: color4,
    hoverControlColor: dark27Color1,

    marker: {
      color: dark27Color1,
      fill: color2
    },
    sidebar: {
      nav: {
        color: dark27Color1,
        backgroundColor: color3,
        hoverColor: dark27Color1,
        hoverBackgroundColor: lighten(0.14, color3)
      }
    },
    toolbar: {
      button: {
        color: dark27Color1,
        backgroundColor: color2,
        hoverBackgroundColor: color3,
        hoverBorderColor: color3,
        focusBackgroundColor: color3,
        focusBorderColor: color2,
        activeBackgroundColor: color2,
        activeBorderColor: color2
      }
    },
    titlebar: {
      color: dark27Color1,
      backgroundColor: color2,

      button: {
        color: dark27Color1,
        backgroundColor: color2,
        hoverBackgroundColor: color3,
        focusBackgroundColor: color3,
        activeBackgroundColor: color3
      }
    },
    form: {
      button: {
        color: dark27Color1,
        backgroundColor: color4,
        hoverBackgroundColor: color5,
        focusBackgroundColor: color5,
        activeBackgroundColor: colors.white,
        outlineColor: colors.white,

        primary: {
          color: color4,
          backgroundColor: dark22Color1,
          hoverBackgroundColor: dark18Color1,
          focusBackgroundColor: dark18Color1,
          activeBackgroundColor: dark22Color1,
          outlineColor: dark27Color1
        },

        link: {
          color: dark27Color1,
          outlineColor: color1
        }
      },
      label: {
        color: colors.white,
        backgroundColor: color2,
        borderColor: dark27Color1,
        hoverBackgroundColor: color3,
        hoverBorderColor: dark18Color1,
        focusBackgroundColor: color3,
        focusBorderColor: dark18Color1,
        activeBackgroundColor: color3,
        activeBorderColor: dark18Color1
      },
      checkbox: {
        color: colors.white,
        backgroundColor: color2,
        borderColor: dark27Color1,
        hoverBackgroundColor: color3,
        hoverBorderColor: dark18Color1,
        focusBackgroundColor: color3,
        focusBorderColor: dark18Color1,
        activeBackgroundColor: color3,
        activeBorderColor: dark18Color1
      }
    },
    modal: {
      color: dark27Color1,
      backgroundColor: color2
    },
    list: {
      color: dark27Color1,
      backgroundColor: color2,
      borderColor: dark27Color1
    }
  });
}

export default {
  buildDarkThemeConfig,
  buildLightThemeConfig
};
