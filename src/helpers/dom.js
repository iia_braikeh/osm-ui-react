export function isOverflowYHidden(element) {
  return window.getComputedStyle(element).overflowY !== 'hidden';
}
