import raw from '!!raw-loader!./pointerClassicThin.svg';
import component from '!!svg-react-loader!./pointerClassicThin.svg';

export default {
  iconAnchor: [25, 7],
  raw,
  component
};
