import raw from '!!raw-loader!./pointerCirclePin.svg';
import component from '!!svg-react-loader!./pointerCirclePin.svg';

export default {
  iconAnchor: [25, 7],
  raw,
  component
};
