import raw from '!!raw-loader!./basicDownTriangle.svg';
import component from '!!svg-react-loader!./basicDownTriangle.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
