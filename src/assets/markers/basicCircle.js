import raw from '!!raw-loader!./basicCircle.svg';
import component from '!!svg-react-loader!./basicCircle.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
