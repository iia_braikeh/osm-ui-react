import raw from '!!raw-loader!./basicLeftTriangle.svg';
import component from '!!svg-react-loader!./basicLeftTriangle.svg';

export default {
  iconAnchor: [25, 25],
  raw,
  component
};
