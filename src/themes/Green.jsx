import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'green'),
  {
    form: {
      button: {
        success: {
          color: colors.white,
          backgroundColor: colors.green3,
          borderColor: colors.green3,
          hoverBackgroundColor: colors.green1,
          hoverBorderColor: colors.green1,
          focusBackgroundColor: colors.green1,
          focusBorderColor: colors.green1,
          activeBackgroundColor: colors.green1,
          activeBorderColor: colors.green1
        }
      }
    }
  }
);

const GreenTheme = themeFactory(config);

GreenTheme.propTypes = {
  children: PropTypes.node.isRequired
};

GreenTheme.displayName = 'GreenTheme';

export default GreenTheme;
