import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildLightThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildLightThemeConfig(defaultThemeConfig, colors, 'orange'),
  {
    form: {
      button: {
        // warning: {
        //   color: colors.white,
        //   backgroundColor: colors.orange1,
        //   borderColor: colors.orange1,
        //   hoverBackgroundColor: colors.orange3,
        //   hoverBorderColor: colors.orange3,
        //   focusBackgroundColor: colors.orange3,
        //   focusBorderColor: colors.orange3,
        //   activeBackgroundColor: colors.orange3,
        //   activeBorderColor: colors.orange3
        // }
      }
    }
  }
);

const OrangeTheme = themeFactory(config);

OrangeTheme.propTypes = {
  children: PropTypes.node.isRequired
};

OrangeTheme.displayName = 'OrangeTheme';

export default OrangeTheme;
