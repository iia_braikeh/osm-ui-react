import PropTypes from 'prop-types';
import { lighten, darken } from 'polished';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildLightThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildLightThemeConfig(defaultThemeConfig, colors, 'lightGray'),
  {
    sidebar: {
      nav: {
        color: darken(0.27, colors.lightGray1),
        backgroundColor: lighten(0.05, colors.lightGray3),
        hoverColor: darken(0.27, colors.lightGray1),
        hoverBackgroundColor: lighten(0.05, colors.lightGray4)
      }
    }
  }
);

const LightGrayTheme = themeFactory(config);

LightGrayTheme.propTypes = {
  children: PropTypes.node.isRequired
};

LightGrayTheme.displayName = 'LightGrayTheme';

export default LightGrayTheme;
