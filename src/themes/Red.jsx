import PropTypes from 'prop-types';
import _merge from 'lodash/merge';
import { colors } from 'constants/index';
import { buildDarkThemeConfig, themeFactory } from 'helpers/themes';
import { config as defaultThemeConfig } from './Default';

export const config = _merge(
  {},
  buildDarkThemeConfig(defaultThemeConfig, colors, 'red'),
  {
    form: {
      button: {
        danger: {
          color: colors.white,
          backgroundColor: colors.red1,
          borderColor: colors.red1,
          hoverBackgroundColor: colors.red3,
          hoverBorderColor: colors.red3,
          focusBackgroundColor: colors.red3,
          focusBorderColor: colors.red3,
          activeBackgroundColor: colors.red3,
          activeBorderColor: colors.red3
        }
      }
    }
  }
);

const RedTheme = themeFactory(config);

RedTheme.propTypes = {
  children: PropTypes.node.isRequired
};

RedTheme.displayName = 'RedTheme';

export default RedTheme;
