import PropTypes from 'prop-types';
import { injectGlobal } from 'styled-components';
import { darken } from 'polished';
import { makeTransparent, themeFactory } from 'helpers/themes';
import { colors } from 'constants/index';

const boxShadow =
  '0 3px 4px -3px rgba(0, 0, 0, 0.5), 0 0 2px -1px rgba(0, 0, 0, 0.4)';

export const config = {
  rem: 16,
  boxShadow,
  font:
    '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif',
  color: colors.text,
  backgroundColor: colors.white,
  borderColor: colors.lightGray4,
  loaderColor: colors.lightGray1,
  borderStyle: 'solid',
  borderWidth: '4px',
  borderRadius: '2px',
  controlColor: colors.lightGray4,
  hoverControlColor: colors.lightGray1,

  map: {
    controlColor: colors.text,
    controlBackgroundColor: colors.white,
    hoverControlColor: colors.white,
    hoverControlBackgroundColor: colors.blue2
  },
  marker: {
    color: colors.text,
    fill: colors.white
  },
  sidebar: {
    boxShadow,
    fontSize: '1rem',
    lineHeight: '1.5rem',
    nav: {
      fontSize: '1.2rem',
      lineHeight: '1.5',
      fontWeight: 500,
      borderRadius: '4px',
      color: colors.text,
      backgroundColor: colors.lightGray5,
      hoverColor: colors.text,
      hoverBackgroundColor: colors.lightGray4
    }
  },
  toolbar: {
    boxShadow,
    margin: '1rem',
    childrenMargin: '.6rem',
    xsSize: '1.8rem',
    smSize: '2.2rem',
    mdSize: '2.6rem',
    lgSize: '3rem',

    xsLineHeight: '1.65rem',
    smLineHeight: '2rem',
    mdLineHeight: '2.5rem',
    lgLineHeight: '2.9rem',

    xsFontSize: '0.65rem',
    smFontSize: '0.7rem',
    mdFontSize: '0.85rem',
    lgFontSize: '0.9rem',

    xsIconSize: '0.65rem',
    smIconSize: '0.7rem',
    mdIconSize: '0.85rem',
    lgIconSize: '0.9rem',

    button: {
      color: colors.text,
      backgroundColor: colors.white,
      borderRadius: '4px',
      borderStyle: 'solid',
      borderWidth: '2px',
      hoverBorderColor: colors.lightGray5,
      hoverBackgroundColor: colors.lightGray5,
      focusBorderColor: colors.lightGray4,
      focusBackgroundColor: colors.lightGray5,
      activeBorderColor: colors.lightGray4,
      activeBackgroundColor: colors.lightGray4
    }
  },
  titlebar: {
    borderRadius: '3px',
    boxShadow,
    minWidth: '400px',
    xsHeight: '2rem',
    smHeight: '2.2rem',
    mdHeight: '2.5rem',
    lgHeight: '2.8rem',
    xsFontSize: '0.9rem',
    smFontSize: '0.95rem',
    mdFontSize: '1rem',
    lgFontSize: '1.1rem',
    fontWeight: 500,
    color: colors.text,
    backgroundColor: colors.white,
    borderColor: colors.lightGray4,
    borderStyle: 'solid',
    floatMargin: '1rem',
    horizontalPadding: '2rem',

    button: {
      color: colors.text,
      backgroundColor: colors.lightGray5,
      borderRadius: '4px',
      hoverBackgroundColor: colors.lightGray4,
      focusBackgroundColor: colors.lightGray4,
      activeBackgroundColor: colors.lightGray4
    }
  },
  form: {
    select: {
      optionColor: colors.text,
      optionBackgroundColor: colors.xhite,
      focusedOptionColor: colors.white,
      focusedOptionBackgroundColor: colors.blue2
    },
    button: {
      fontWeight: 500,
      color: colors.anthracite2,
      borderRadius: '4px',
      backgroundColor: colors.lightGray5,
      hoverBackgroundColor: colors.lightGray4,
      focusBackgroundColor: colors.lightGray4,
      activeBackgroundColor: colors.lightGray3,
      outlineColor: colors.lightGray3,

      primary: {
        fontWeight: 500,
        color: colors.white,
        backgroundColor: colors.blue2,
        hoverBackgroundColor: colors.blue3,
        focusBackgroundColor: colors.blue3,
        activeBackgroundColor: colors.blue2,
        outlineColor: colors.blue2
      },

      info: {
        fontWeight: 500,
        color: darken(0.27, colors.turquoise1),
        backgroundColor: colors.turquoise2,
        hoverBackgroundColor: colors.turquoise3,
        focusBackgroundColor: colors.turquoise3,
        activeBackgroundColor: colors.turquoise2,
        outlineColor: colors.turquoise2
      },

      success: {
        fontWeight: 500,
        color: colors.white,
        backgroundColor: colors.green2,
        hoverBackgroundColor: colors.green3,
        focusBackgroundColor: colors.green3,
        activeBackgroundColor: colors.green2,
        outlineColor: colors.green2
      },

      warning: {
        fontWeight: 500,
        color: darken(0.27, colors.orange1),
        backgroundColor: colors.orange2,
        hoverBackgroundColor: colors.orange3,
        focusBackgroundColor: colors.orange3,
        activeBackgroundColor: colors.orange2,
        outlineColor: colors.orange2
      },

      danger: {
        fontWeight: 500,
        color: colors.white,
        backgroundColor: colors.red2,
        hoverBackgroundColor: colors.red3,
        focusBackgroundColor: colors.red3,
        activeBackgroundColor: colors.red2,
        outlineColor: colors.red2
      },

      link: {
        fontWeight: 400,
        color: colors.blue2,
        backgroundColor: 'transparent',
        hoverBackgroundColor: 'transparent',
        focusBackgroundColor: 'transparent',
        activeBackgroundColor: 'transparent',
        outlineColor: colors.blue2
      }
    },
    input: {
      fontSize: '.9rem',
      lineHeight: '1.2rem',
      borderWidth: '1px',
      borderRadius: '2px',
      boxShadow: 'inset 0 1px 1px rgba(0, 0, 0, 0.075)',
      opacity: 1,
      backgroundColor: colors.white,
      borderColor: colors.lightGray3,
      disabledOpacity: 0.6,

      focusOpacity: 1,
      focusBackgroundColor: colors.white,
      focusBorderColor: colors.blue2,
      focusBoxShadow: 'inset 0 1px 1px rgba(0, 0, 0, 0.075)',

      info: {
        color: colors.turquoise2,
        backgroundColor: colors.white,
        borderColor: colors.turquoise2
      },

      success: {
        color: colors.green2,
        backgroundColor: colors.white,
        borderColor: colors.green2
      },

      warning: {
        color: colors.orange2,
        backgroundColor: colors.white,
        borderColor: colors.orange2
      },

      error: {
        color: colors.red1,
        backgroundColor: colors.white,
        borderColor: colors.red2
      }
    },
    label: {
      color: colors.text,
      borderColor: colors.text,
      backgroundColor: colors.white,
      fontSize: '1rem',
      fontWeight: 400,
      lineHeight: '1.6rem',

      info: {
        color: colors.turquoise1
      },

      success: {
        color: colors.green1
      },

      warning: {
        color: colors.orange1
      },

      error: {
        color: colors.red1
      }
    },
    checkbox: {
      borderColor: colors.darkGray3,
      color: colors.darkGray2,
      disabledOpacity: 0.6,

      sm: {
        lineHeight: '1.8rem',
        size: '0.95rem',
        top: '0.35rem',
        innerSize: '0.55rem',
        innerTop: '-0.1rem',
        paddingLeft: '1.6rem'
      },
      md: {
        lineHeight: '2rem',
        size: '1.05rem',
        top: '0.45rem',
        innerSize: '0.7rem',
        innerTop: '-0.1rem',
        paddingLeft: '1.8rem'
      },
      lg: {
        lineHeight: '2.8rem',
        size: '1.25rem',
        top: '0.65rem',
        innerSize: '0.8rem',
        innerTop: '-0.15rem',
        paddingLeft: '2rem'
      }
    },
    radio: {
      borderColor: colors.darkGray3,
      backgroundColor: colors.darkGray2,
      disabledOpacity: 0.6,

      sm: {
        lineHeight: '1.8rem',
        size: '0.95rem',
        top: '0.4rem',
        innerSize: '0.55rem',
        innerTop: '0.6rem',
        innerLeft: '0.2rem',
        paddingLeft: '1.6rem'
      },
      md: {
        lineHeight: '2rem',
        size: '1.05rem',
        top: '0.4rem',
        innerSize: '0.55rem',
        innerTop: '0.65rem',
        innerLeft: '0.25rem',
        paddingLeft: '1.8rem'
      },
      lg: {
        lineHeight: '2.8rem',
        size: '1.25rem',
        top: '0.75rem',
        innerSize: '0.75rem',
        innerTop: '1rem',
        innerLeft: '0.25rem',
        paddingLeft: '2rem'
      }
    },
    hint: {
      fontSize: '.8rem',
      margin: 0,
      padding: '.4rem .8rem',
      disabledOpacity: 0.6,
      backgroundColor: colors.lightGray5,
      color: colors.anthracite2,

      info: {
        color: darken(0.27, colors.turquoise1),
        backgroundColor: colors.turquoise2
      },

      success: {
        color: colors.white,
        backgroundColor: colors.green2
      },

      warning: {
        color: darken(0.27, colors.orange1),
        backgroundColor: colors.orange2
      },

      error: {
        color: colors.white,
        backgroundColor: colors.red2
      }
    }
  },
  alert: {
    borderRadius: '2px',
    horizontalPadding: '1.5rem',
    verticalPadding: '1.2rem',

    info: {
      color: darken(0.18, colors.turquoise1),
      backgroundColor: colors.turquoise5
    },

    success: {
      color: darken(0.18, colors.green1),
      backgroundColor: colors.green5
    },

    warning: {
      color: darken(0.18, colors.orange1),
      backgroundColor: colors.orange5
    },

    danger: {
      color: darken(0.18, colors.red1),
      backgroundColor: colors.red5
    }
  },
  modal: {
    color: colors.text,
    overlayBackgroundColor: makeTransparent(colors.black, 0.65),
    overlayPadding: '4rem',
    backgroundColor: colors.white,
    padding: '3.5rem 2rem'
  },
  notification: {
    fontSize: '1rem',
    offset: '1rem',
    width: '35rem',
    height: '3rem',

    info: {
      color: darken(0.18, colors.turquoise1),
      backgroundColor: colors.turquoise5,
      controlColor: colors.turquoise2,
      hoverControlColor: colors.turquoise3
    },

    success: {
      color: darken(0.18, colors.green1),
      backgroundColor: colors.green5,
      controlColor: colors.green2,
      hoverControlColor: colors.green3
    },

    warning: {
      color: darken(0.18, colors.orange1),
      backgroundColor: colors.orange5,
      controlColor: colors.orange2,
      hoverControlColor: colors.orange3
    },

    danger: {
      color: darken(0.18, colors.red1),
      backgroundColor: colors.red5,
      controlColor: colors.red2,
      hoverControlColor: colors.red3
    }
  },
  list: {
    fontSize: '.9rem',
    color: colors.anthracite2,
    backgroundColor: colors.white,
    borderColor: colors.lightGray3
  }
};

/* eslint-disable no-unused-expressions */
injectGlobal`
    *, *:before, *:after {
      box-sizing: border-box;
    }

    html, body {
      font-family: ${config.font};
      font-size: ${config.rem}px;
      line-height: 1.5;
      color: ${config.color};
    }

    .osm-ui-react-marker-default,
    .osm-ui-react-marker-white {
      color: ${config.color};

      #colorized, #colorized * {
          fill: ${colors.white} !important;
      }
    }

    .osm-ui-react-marker-cluster-default > div {
      color: ${config.color};
      background: ${colors.white};
    }

    /*
      TODO: Remove that dirty patch when the following issue is fixed:
      https://github.com/Leaflet/Leaflet/issues/4578
    */
    .leaflet-fade-anim .leaflet-tile,
    .leaflet-zoom-anim .leaflet-zoom-animated {
      will-change: auto !important;
    }
`;
/* eslint-enable */

const DefaultTheme = themeFactory(config);

DefaultTheme.propTypes = {
  children: PropTypes.node.isRequired
};

DefaultTheme.displayName = 'DefaultTheme';

export default DefaultTheme;
