import { configure, setAddon } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import infoAddon from '@storybook/addon-info';

setAddon(infoAddon);

setOptions({
  name: 'OSM UI - REACT',
  url: 'https://gitlab.com/osm-ui/react',
  goFullScreen: false,
  showSearchBox: false,
  showStoriesPanel: true,
  showAddonPanel: true,
  addonPanelInRight: true,
  sortStoriesByKind: false,
});

function loadStories() {
  /* eslint-disable */
  require('../src/helpers/__stories__/Introduction');
  require('../src/components/Alert/__stories__/Alert');
  require('../src/components/Button/__stories__/Button');
  require('../src/components/List/__stories__/List');
  require('../src/components/Form/__stories__/Form');
  require('../src/components/Loader/__stories__/Loader');
  require('../src/components/Sidebar/__stories__/Sidebar');
  require('../src/components/Toolbar/__stories__/Toolbar');
  require('../src/components/Titlebar/__stories__/Titlebar');
  require('../src/components/OffMapMarker/__stories__/OffMapMarker');
  require('../src/components/Map/__stories__/Map');
  require('../src/components/Modal/__stories__/Modal');
  require('../src/components/Notification/__stories__/Notification');
  require('../src/components/Osmose/__stories__/Osmose');
  require('../src/components/Osm/__stories__/Osm');
  require('../src/components/ColorPicker/__stories__/ColorPicker');
  require('../src/components/IconPicker/__stories__/IconPicker');
  require('../src/components/Scrollable/__stories__/Scrollable');
  /* eslint-enable */
}

configure(loadStories, module);
